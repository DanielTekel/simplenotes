# SimpleNotes app  
## for BSC interview process  

A very simple application that handles notes:  
- minimalistic design and functionality - allows listing, creating, updating, deleting notes,  
- works with apiary backend,  
- written in Swift 5 language,  
- uses Alamofire framework,  
- dependencies handled by cocoa pods (pod file included in repo),  
- contains few simple unit tests,  
- developed with XCode 10.2.1 + iOS SDK 12.2.  

### Building the app:  
1. checkout repo `git clone https://DanielTekel@bitbucket.org/DanielTekel/simplenotes.git`  
2. install cocoa pods `pod install`  
3. open `SimpleNotes.xcworkspace`  
4. configure code signing (has automtic code signing management turned on by default)  
5. (hopefully ;)) build and run!
