//
//  NotesApiaryMockDataProvider.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit
import Alamofire

let BACK_END_BASE_URL: String = "https://private-9aad-note10.apiary-mock.com/"
let BACK_END_NOTES_URL: String = BACK_END_BASE_URL + "notes"
let BACK_END_NOTE_BY_ID_URL_FORMAT: String = BACK_END_NOTES_URL + "/%d"


class ApiaryMockNotesDataAPI: NotesDataAPI {
    
    func loadAll(completion: @escaping (Data?, Error?) -> Void) {
        Alamofire.request(BACK_END_NOTES_URL).response { (response) in
            completion(response.data, response.error)
        }
    }
    
    func save(note: Note, completion: @escaping (Error?) -> Void) {
        let URLString = (note.id == nil ? BACK_END_NOTES_URL : String(format: BACK_END_NOTE_BY_ID_URL_FORMAT, note.id!));       // creating new note or updating existing one
        var request = URLRequest(url: URL(string: URLString)!)
        request.httpMethod = (note.id == nil ? HTTPMethod.post.rawValue : HTTPMethod.put.rawValue)                              // creating new note or updating existing one
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var JSONdata: Data? = nil
        let encoder = JSONEncoder()
        do {
            JSONdata = try encoder.encode(note)
        }
        catch {
            completion(error)
            return
        }
        
        request.httpBody = JSONdata
        
        Alamofire.request(request).response { (response) in
            if response.error != nil {
                completion(response.error)
                return
            }
            
            guard let data = response.data else {
                completion(response.error)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let noteFromServer = try decoder.decode(Note.self, from: data)
                
                note.id = noteFromServer.id               // id now set
//                note.title = noteFromServer.title         // unnecessary
                
                completion(nil)
            }
            catch {
                completion(error)
            }
        }
    }
    
    func delete(note: Note, completion: @escaping (Error?) -> Void) {
        if note.id == nil {
            // not saved yet - no need to delete
            completion(nil)
            return
        }
        
        Alamofire.request(String(format: BACK_END_NOTE_BY_ID_URL_FORMAT, note.id!), method: .delete).response { (response) in
            if response.error != nil {
                completion(response.error)
            }
            else {
                if let data = response.data, data.count > 0 {
                    if let responseString = String(data: data, encoding: .utf8) {
                        print("got delete response: "+responseString)
                    }
                }
                
                completion(nil)
            }
        }
    }

}
