//
//  Note.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit
import Alamofire

/**
 Note model class
 allows creating new notes, updating, deleting via NotesManager
 */
class Note: Codable {
    var id: Int?                    // nil for Notes that have not been sent to server yet
    var title: String?              // may be title from server can come nil?
    
    init(_ title: String) {           // notes with nil titles are not allowed
        self.id = nil
        self.title = title
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        if self.id != nil {         // when encoding a new note (without id), id field should not be in the JSON
            try container.encode(id, forKey: .id)
        }
        try container.encode(title, forKey: .title)
    }
    
    
    func save(completion: @escaping (Error?) -> Void) {
        NotesManager.shared.save(note: self, completion: completion)
    }
    
    func delete(completion: @escaping (Error?) -> Void) {
        NotesManager.shared.delete(note: self, completion: completion)
    }
}

