//
//  NotesDataProvier.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit

/**
 protocol for data "handlers" of Note objects
 */
protocol NotesDataAPI {
    
    func loadAll(completion: @escaping (Data?, Error?) -> Void)
    func save(note: Note, completion: @escaping (Error?) -> Void)
    func delete(note: Note, completion: @escaping (Error?) -> Void)
    
}
