//
//  NotesManager.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit

/**
 Main class for managing Note objects
 singleton - use `shared` property to access singleton instance. This is because I don't expect there'll be multiple data sources used in a single app...
 `dataAPI` property has to be initialized before calling any methods! This restriction is because of coding convenience (together with singleton pattern)
 */
class NotesManager {
    
    /**
     returned when `dataAPI` property has not been initialized
     */
    class NoDataDelegateError: Error {
        var localizedDescription: String = "NoteManager has not been initialized with data provider"
    }
    
    
    static let shared = NotesManager()              // shared singleton instance
    var dataAPI: NotesDataAPI?
    
    
    private init() {}                               // force singleton access

    /**
     loads all notes from data API
     parsed `Note` objects are passed to completion handler
     in case an error occures, it's passed to completion handler
    */
    func loadAll(completion: @escaping ([Note]?, Error?) -> Void) {
        guard let dataAPI = self.dataAPI else {
            completion(nil, NoDataDelegateError())
            return
        }
        
        dataAPI.loadAll { (data, error) in
            if error != nil {
                completion(nil, error)
                return
            }
            
            guard let data = data else {
                completion(nil, error)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let notes = try decoder.decode([Note].self, from: data)
                completion(notes, nil)
            }
            catch {
                completion(nil, error)
            }
        }
    }
    
    /**
     saves one note via data API
     in case an error occures, it's passed to completion handler
    */
    func save(note: Note, completion: @escaping (Error?) -> Void) {
        guard let dataAPI = self.dataAPI else {
            completion(NoDataDelegateError())
            return
        }
        
        dataAPI.save(note: note, completion: completion)
    }
    
    /**
     deletes one note via data API
     in case an error occures, it's passed to completion handler
     */
    func delete(note: Note, completion: @escaping (Error?) -> Void) {
        guard let dataAPI = self.dataAPI else {
            completion(NoDataDelegateError())
            return
        }
        
        dataAPI.delete(note: note, completion: completion)
    }
}
