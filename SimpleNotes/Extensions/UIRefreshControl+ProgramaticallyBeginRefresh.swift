//
//  UIRefreshControl+ProgramaticallyBeginRefresh.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit

extension UIRefreshControl {
    
    func programaticallyBeginRefreshing(in tableView: UITableView) {            // because calling beginRefreshing() on UIRefreshControl doesn't show it in the table view - tableView needs to be scrolled down
        beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: tableView.contentOffset.y-frame.size.height)
        tableView.setContentOffset(offsetPoint, animated: true)
    }
    
}
