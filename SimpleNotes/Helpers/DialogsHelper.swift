//
//  DialogsHelper.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit

class DialogsHelper {
    
    private static var pleaseWaitAlertController: UIAlertController?
    
    static func displayErrorDialog(inViewController viewController: UIViewController, withMessage message: String, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        DispatchQueue.main.async {
            viewController.present(alert, animated: true, completion: completion)
        }
    }
    
    static func displayPleaseWaitDialog(inViewController viewController: UIViewController, completion: (() -> Void)? = nil) {
        if pleaseWaitAlertController == nil {
            pleaseWaitAlertController = UIAlertController(title: "Please wait", message: "Communicating with back-end...", preferredStyle: .alert)
        }
        
        DispatchQueue.main.async {
            viewController.present(pleaseWaitAlertController!, animated: true, completion: completion)
        }
    }
    
    static func dismissPleaseWaitDialog(completion: (() -> Void)? = nil) {
        if let alertController = pleaseWaitAlertController {
            DispatchQueue.main.async {
                alertController.dismiss(animated: true, completion: completion)
            }
        }
    }
    
}
