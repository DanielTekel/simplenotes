//
//  DetailViewController.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var noteTextView: UITextView!
    
    var note: Note? {
        didSet {
            // Update the view
            configureView()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    func configureView() {
        if noteTextView != nil {                // not loaded before displaying
            if let noteText = note?.title {
                DispatchQueue.main.async {      // could be called from BG thread
                    self.noteTextView.text = noteText
                }
            }
        }
        
        DispatchQueue.main.async {
            if self.note == nil {
                // creating new note
                
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain,target: self, action: #selector(self.saveNewBtnPressed))
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelBtnPressed))
                
                self.title = "New Note"
            }
            else {
                // updating existing note
                
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(self.trashBtnPressed))
                self.navigationItem.leftBarButtonItem = nil
                self.navigationItem.hidesBackButton = false
            }
        }
    }
    
    // MARK: - button actions
    
    @objc
    func saveNewBtnPressed() {
        self.noteTextView.resignFirstResponder()
        
        let note = Note(noteTextView.text)

        DialogsHelper.displayPleaseWaitDialog(inViewController: self) {
            note.save { (error) in
                DialogsHelper.dismissPleaseWaitDialog() {
                    if error != nil {
                        DialogsHelper.displayErrorDialog(inViewController: self, withMessage: "An error occured while saving your note: "+error!.localizedDescription)
                    }
                    else {
                        DispatchQueue.main.async {
                            self.dismiss(animated: true)
                        }
                    }
                }
            }
        }
    }
    
    @objc
    func updateBtnPressed() {
        self.noteTextView.resignFirstResponder()
        
        if let note = self.note {
            note.title = self.noteTextView.text
            
            DialogsHelper.displayPleaseWaitDialog(inViewController: self) {
                note.save { (error) in
                    DialogsHelper.dismissPleaseWaitDialog() {
                        if error != nil {
                            DialogsHelper.displayErrorDialog(inViewController: self, withMessage: "An error occured while saving your note: "+error!.localizedDescription)
                        }
                        else {
                            DispatchQueue.main.async {
                                self.navigationController?.navigationController?.popViewController(animated: true)
                            }
                        }
                        
                        self.configureView()
                    }
                }
            }
        }
    }
    
    @objc
    func cancelBtnPressed() {
        self.dismiss(animated: true)
    }
    
    @objc
    func cancelUpdatesBtnPressed() {
        configureView()
        self.noteTextView.resignFirstResponder()
    }
    
    @objc
    func trashBtnPressed() {
        if let note = self.note {
            DialogsHelper.displayPleaseWaitDialog(inViewController: self) {
                note.delete { (error) in
                    DialogsHelper.dismissPleaseWaitDialog() {
                        if error != nil {
                            DialogsHelper.displayErrorDialog(inViewController: self, withMessage: "An error occured while deleting your note: "+error!.localizedDescription)
                        }
                        else {
                            DispatchQueue.main.async {
                                self.navigationController?.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Text view delegate
    
    func textViewDidChange(_ textView: UITextView) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain,target: self, action: #selector(updateBtnPressed))
        navigationItem.hidesBackButton = true
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelUpdatesBtnPressed))
    }


}

