//
//  MasterViewController.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit

class NotesListViewController: UITableViewController {

    var detailViewController: NoteViewController? = nil
    var notes = [Note]()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // navbar action buttons initialization
        navigationItem.leftBarButtonItem = editButtonItem
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBtnPressed))
        navigationItem.rightBarButtonItem = addButton
        
        // initializing detail controller
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? NoteViewController
        }
        
        // refresh control for reloading notes
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    @objc
    func addBtnPressed() {
        if let viewController = detailViewController {
            detailViewController?.note = nil
            let navCtrl = UINavigationController(rootViewController: viewController)
            self.present(navCtrl, animated: true)
        }
    }
    
    @objc func refresh() {
        DispatchQueue.main.async {
            self.tableView.refreshControl?.programaticallyBeginRefreshing(in: self.tableView)
        }
        
        NotesManager.shared.loadAll { (notes: [Note]?, error: Error?) in
            DispatchQueue.main.async {
                self.tableView.refreshControl?.endRefreshing()
            }
            
            guard error == nil else {
                DialogsHelper.displayErrorDialog(inViewController: self, withMessage: "An error occured while loading notes from back-end: "+error!.localizedDescription)
                return
            }
            
            if notes != nil {
                self.notes = notes!
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let note = notes[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! NoteViewController
                controller.note = note
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let note = notes[indexPath.row]
        cell.textLabel!.text = note.title
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DialogsHelper.displayPleaseWaitDialog(inViewController: self) {
                let note = self.notes[indexPath.row]
                
                note.delete { (error) in
                    DialogsHelper.dismissPleaseWaitDialog() {
                        if error != nil {
                            DialogsHelper.displayErrorDialog(inViewController: self, withMessage: "An error occured while deleting note: "+error!.localizedDescription)
                        }
                        else {
                            DispatchQueue.main.async {
                                self.notes.remove(at: indexPath.row)
                                self.tableView.deleteRows(at: [indexPath], with: .fade)
                            }
                        }
                    }
                }
            }
        }
    }


}

