//
//  NotesLocalFileDataProvider.swift
//  SimpleNotes
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import UIKit

let testJsonFileName = "testNotes.json"                     // this file has to be bundled with the app

/**
 A mock-up of real data back-end.
 supports only loading notes from local JSON file
*/
class LocalFileNotesDataAPI: NotesDataAPI {
    
    private class TestFileNotFoundError: Error {
        var localizedDescription: String = "could not load test data from file "+testJsonFileName
    }
    
    func loadAll(completion: @escaping (Data?, Error?) -> Void) {
        // loads data from JSON file and passes to NotesManager for parsing
        
        if let url = Bundle.main.url(forResource: testJsonFileName, withExtension: "")
        {
            do {
                completion(try Data(contentsOf: url), nil)
            }
            catch {
                completion(nil, error)
            }
        }
        else {
            completion(nil, TestFileNotFoundError())        // should (almost ;)) never occur
        }
    }
    
    func save(note: Note, completion: @escaping (Error?) -> Void) {
        // not supported
        completion(nil)
    }
    
    func delete(note: Note, completion: @escaping (Error?) -> Void) {
        // not supported
        completion(nil)
    }

}
