//
//  SimpleNotesTests.swift
//  SimpleNotesTests
//
//  Created by Tekel, Daniel on 04/10/2019.
//  Copyright © 2019 Daniel Tekel. All rights reserved.
//

import XCTest
@testable import SimpleNotes

/**
 few simple unit tests on data layer
 */
class SimpleNotesTests: XCTestCase {

    override func setUp() {
    }

    func testNotesLoading() {
        // loads json definition from local file and parses it into an array of Notes - tests notes' JSON parsing
        
        NotesManager.shared.dataAPI = LocalFileNotesDataAPI()       // set up mocked-up local file data API
        
        NotesManager.shared.loadAll { (notes, error) in
            XCTAssert(error == nil && notes != nil)
            XCTAssert(notes?.count == 2)
            XCTAssert(notes?[0].id == 1)
            XCTAssert(notes?[1].title == "Test note 2")
        }
        
        NotesManager.shared.dataAPI = nil                           // clear up after finishing these test
    }
    
    func testNotesManagerInitialization() {
        // checks whether correct error is returned in case NotesManager's dataProvider has not been set up
        
        XCTAssert(NotesManager.shared.dataAPI == nil)
        
        NotesManager.shared.loadAll { (notes, error) in
            XCTAssert(notes == nil && error is NotesManager.NoDataDelegateError)
        }
        
        NotesManager.shared.save(note: Note("test"), completion: { (error) in
            XCTAssert(error is NotesManager.NoDataDelegateError)
        })
        
        NotesManager.shared.delete(note: Note("test")) { (error) in
            XCTAssert(error is NotesManager.NoDataDelegateError)
        }
    }

}
